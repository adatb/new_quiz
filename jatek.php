<?php
	include_once('database.php');
    
    if (!isset($_SESSION['username'])) {
        header('Location: index.php');
    }
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Quiz - Játék</title>
        <link rel="stylesheet" href="style.css"/>
    </head>
    <body>
        <?php
            require_once("functions.php");
                @$temakor = htmlspecialchars($_POST['temakor']);
                @$nehezseg = $nehez_toint[htmlspecialchars($_POST['nehezseg'])];
				echo($temakor);

                $lek = '?temakor=' . $temakor . '&nehezseg=' . $nehezseg;
                echo "<form method=\"post\" action='jatek.php$lek'>";
				
				$query = "SELECT * FROM (SELECT * FROM kerdes WHERE szint=$nehezseg AND temakor_id=$temakor ORDER BY dbms_random.value) WHERE rownum <= 2";
				$s = oci_parse($c, $query );
				$result = oci_execute($s);
				if ($result === NULL) {
                    adatbazisHiba();
                    die();
                }
				
				$row = array();
                while ($kerdes = oci_fetch_array($s)){
					$row[]=$kerdes;
				}
				
				foreach ($row as $kerdes) {
					echo ("<p>$kerdes[1]</p>");
					$s2 = oci_parse($c, "SELECT * FROM Valasz WHERE kerdes_id=$kerdes[0]" );
					oci_execute($s2);
					while ($valasz = oci_fetch_array($s2)){
						echo ("<input type='radio' name='$kerdes[0]' value='$valasz[0]'>$valasz[1]<br>");
					}
				}
                echo '<input type="submit" value="Válaszok véglegesítése" name="valasz" id="valasz"/>';
                echo '</form>';
                if (!isset($_GET['temakor'])) { //amikor játszunk, egy percünk van válaszolni a kiadott kérdésekre, az értékeléskor ez a blokk már nem fut le
                   // header('refresh: 60, url=notime.php');
                }
        ?>
        <?php
            if (isset($_POST['valasz'])) {
                $temakor = $_GET['temakor'];
                $nehezseg = $_GET['nehezseg'];
                
				$max = 5 + 5 * $nehezseg;
				
                $hanyJoValasz = 0;
                $hanyRosszValasz = 0;
                /* Gyakorlatilag a for ciklusban úgy járom be a dolgokat, hogy veszed a i-edik radio button értékét (ez a válasz, amit a játékos bejelölt),
                 * illetve a $joValasz tömb i-edik elemét, amely pedig az ahhoz a kérdéshez tartozó jó válasz. Ezek után összehasonlítom őket, és egy számlálót
                 * növelek (a jóválaszok számának megfelelő pontszámot fog kapni a játékos, legalábbis én így képzeltem).
                 */
                for ($i = 1; $i <= $max; $i++) {
                    $a = $_POST["$i"];
                    if ($a === $joValasz[$i-1]) {
                        $hanyJoValasz++;
                        break;
                    } else {
                        $hanyRosszValasz--;
                        break;
                    }
                }
				$hanyJoValasz*=$nehezseg;
				$un = $_SESSION['username'];
				$query = "insert into pont VALUES (NULL,'$un',0,$hanyJoValasz,SYSDATE)";
				$s = oci_parse($c, $query );
				$result = oci_execute($s);
                if ($result === null) {
                    adatbazisHiba();
                    die();
                }
            }
        ?>
    </body>
</html>