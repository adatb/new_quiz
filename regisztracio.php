<script>
    document.title = "Quiz - Regisztráció";
</script>

<link rel="stylesheet" href="jquery-ui-1.11.4.custom/jquery-ui.css">
<script src="jquery-ui-1.11.4.custom/external/jquery/jquery.js"></script>
<script src="jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<script>
	$(function() {
		$( "#date" ).datepicker();
	});
</script>

<form id="regisztracio" action="index.php?op=regisztracio" method="post">
	<label for="username">Felhasználónév:</label>
	<input type="text" name="username" id="username"/>
	<label for="pass">Jelszó:</label>
	<input type="password" name="pass" id="pass"/>
	<label for="pass2">Jelszó mégegyszer:</br></label>
	<input type="password" name="pass2" id="pass2"/>
	<table id="gender">
	<tr>
	<td><label for="gender"> Nem (Fiú, lány): </label> 
	<input type="radio" name="gender" id="fiu" value="1"/>
	<input type="radio" name="gender" id="lany" value="0"/></td>
	</tr>
	</table>
	<label for="mail">E-mail cím:</label>
	<input type="text" name="mail" id="mail"/>
	<label for="date">Születési dátum:</label>
	<input type="text" name="date" id="date"/>
	<input type="submit" name="regisztral" id="regisztral" value="Regisztráció"/>
	
</form>
<?php
    //require_once(param), ahol param: a fájl, amiben az adatbázis kapcsolódás van
    require_once("functions.php");
    $mail_regex = '/^([A-z0-9\_\.\-]+)@([a-z0-9\_\-\.]+).([a-z]{2,})$/';
    $helyes = false;
    if (isset($_POST['regisztral'])) {
        //megvan-e adva...
        //... felhasználónév?
        if (empty($_POST['username'])) {
            hibaUzenet('felhasználónév'); //$helyes: false;
        } else {
            $helyes = true;
        }
        //... jelszó?
        if (empty($_POST['pass'])) {
            hibaUzenet('jelszó');         //$helyes: true
            $helyes = false;
        } else {
            $helyes = true;
        }
        //... ismételt jelszó?
        if (empty($_POST['pass2'])) {     //$helyes: true
            hibaUzenet('az ismételt jelszó');
            $helyes = false;
        } else {
            $helyes = true;
        }
		//... neme
		 if (empty($_POST['gender'])) {
            hibaUzenet('nemed'); //$helyes: false;
        } else {
            $helyes = true;
        }
        //... mail?
        if (empty($_POST['mail'])) {
            hibaUzenet('e-mail cím');
            $helyes = false;
        } else {
            if (!preg_match($mail_regex, $_POST['mail'])) {
                rosszFormatum('e-mail cím');
                $helyes = false;
            } else {
                $helyes = true;
            }
        }
    }
    //ha minden meg van adva, további ellenőrzéseket végzünk (pl. hogy létezik-e már a felhasználónév az adatbázisban)
    if ($helyes == true) {
        $username = $_POST['username'];
        $password = $_POST['pass'];
        $password2 = $_POST['pass2'];
        $mail = $_POST['mail'];
        if (strlen($username) < 6) {
            rosszHossz('felhasználónév', 6);
            $helyes = false;
        }

        if (strlen($password) < 8) {
            rosszHossz('jelszó', 8);
            $helyes = false;
        }

        if ($password !== $password2) {
            echo '<div class="error">A két jelszó nem egyezik!</div>';
            $helyes = false;
        }

        if (strlen($mail) > 30) {
            echo '<div class="error">Az e-mail cím maximum 30 karakter lehet</div>';
            $helyes = false;
        }

        if ($helyes == true) {
            //felhasználónév ellenőrzés
			$s = oci_parse($c, "select nev from felhasznalo");
			$result = oci_execute($s);

            if ($result === NULL) {
                adatbazisHiba();
                die();
            }

            //eredmény feldolgozása, while ciklus
            while ($row = oci_fetch_array($s)) {
                if ($username === $row[0]) {
                    foglalt('felhasználónév');
                    $helyes = false;
                }
            }

            //e-mail cím ellenőrzés
            $query = "SELECT email FROM felhasznalo";
            $s = oci_parse($c, $query);
			$result = oci_execute($s);
            if ($result === NULL) {
                adatbazisHiba();
                die();
            }

            while ($row = oci_fetch_array($s)) {
                if ($mail === $row[0]) {
                    foglalt('email cím');
                    $helyes = false;
                }
            }
        }

        if ($helyes == true) {
			$md5pass = md5($password);
            //adatok beszúrása az adatbázisba - mire ide érünk, minden valid, nem foglalt a felhasználónév
			$date = $_POST['date'];
			$gender = $_POST['gender'];
            $query = "INSERT INTO felhasznalo VALUES(0, '$username', '$mail', '$md5pass', TO_DATE('$date', 'mm/dd/yyyy'), '$gender', 0)";
            $s = oci_parse($c, $query);
			$result = oci_execute($s);
			
			
            if (!$result) {
                adatbazisHiba();
                die();
            } else {
                echo '<div class="notice">Sikeres regisztráció!</div>';

				if(!isset($_SESSION)) 
				{ 
					session_start(); 
				} 
			
				$query = "select id from felhasznalo where nev='$username'";
				$s = oci_parse($c, $query);
				$result = oci_execute($s);
				$row = oci_fetch_array($s);
                $_SESSION['username'] = $row[0];
            }
        }
    }
}
?>