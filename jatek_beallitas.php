﻿<script>
document.title = "Quiz - Játék beállítások";
</script>
<?php
//nehézség kiválasztása dropdown list-tel
$nehezseg  = '<select id="nehezseg" name="nehezseg">';
$nehezseg .= '<option>Válassz nehézséget!</option>';
$nehezseg .= '<option>Könnyű</option>';
$nehezseg .= '<option>Közepes</option>';
$nehezseg .= '<option>Nehéz</option>';
$nehezseg .= '</select>';		

//témakör kiválasztása dropdown list-tel
//$query: lekérdezés, amely visszaadja az egyes témakörök neveit, hogy azokat beletegyük a dropdown list-be
$s = oci_parse($c, 'select * from temakor');
oci_execute($s);

$temakor  = '<select id="temakor" name="temakor">';
$temakor .= '<option selected disabled>Válassz témakört!</option>';
$temakor .= '<option value=1>Biológia</option>';
while ($row = oci_fetch_array($s, OCI_ASSOC + OCI_RETURN_NULLS)) {   //amíg van sor, $row: aktuális rekord
	$temakor .= "<option value=".$row['ID'].">" . $row['TEMAKOR'] . "</option>"; //aktuális témakör nevet kiteszi egy option tag-be
}
$temakor .= '</select>';

echo '<form id="jatek_beallitas" method="post" action="index.php?op=jatek">';
echo $nehezseg;
echo $temakor;
echo '<input type="submit" name="jatek" id="jatek" value="Játékra fel!"/>';
echo '</form>';
?>
