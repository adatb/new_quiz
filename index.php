<!doctype html>
<html>
    <head>
        <meta charset="utf-8"/>
        <link rel="stylesheet" href="style.css"/>
        <link rel="stylesheet" href="belepes_style.css"/>
        <link rel="stylesheet" href="regisztracio_style.css"/>
		<link rel="stylesheet" href="forum_style.css"/>
        <title>Quiz - Kezdőoldal</title>
    </head>
    <body>
		<div id="menu">
			<ul>
			<?php 
			include_once("database.php"); 
			if(!isset($_SESSION)) {
				session_start();
			}
			if (isset($_SESSION['username'])) {
				?>
			<li><a href="index.php?op=jatek_beallitas">Új játék</a></li>
			<li><a href="index.php?op=ranglista">Ranglista</a></li>
			<li><a href="index.php?op=forum">Fórum</a></li>                  
			<li><a href="index.php?op=kijelentkezes">Kijelentkezés</a></li>
				<?php
				if(isset($_SESSION['admin'])){
					?>
					<li><a href="index.php?op=ujkerdes">Új kérdés</a></li>
					
					<?php
				}
				?> </ul> <?php
			} else {
				?>
				<li><a href="index.php">Főoldal</a></li>
				<li><a href="index.php?op=belepes">Belépés</a></li>
				<li><a href="index.php?op=regisztracio">Regisztráció</a></li>
				<li><a href="index.php?op=miez">Mi ez?</a></a></li>
				<?php
			}
			?>
		</div>
        <div id="content">
            <?php
                $op = isset($_GET['op']) ? $_GET['op'] : null;
                if ($op !== null) {
                    if (file_exists($op . '.php')) {
                        require_once($op . ".php");
                    }
                }
            ?>
        </div>
    </body>
</html>