<?php
    function hibaUzenet($name) {
        echo '<div class="error">Nincs megadva ' . $name . '!</div>';
    }

    function rosszHossz($name, $h) {
        echo '<div class="error">A ' . $name . ' hossza minimum ' . $h . ' karakter kell, hogy legyen!</div>';
    }

    function foglalt($name) {
        echo '<div class="error">A megadott ' . $name . ' már foglalt!</div>';
    }

    function adatbazisHiba() {
        echo '<div class="error">Adatbázis hiba!</div>';
    }

    function rosszFormatum($name) {
        echo '<div class="error">Az ' . $name . ' nem megfelelő formátumú!</div>';
    }

    function kerdesKiiras($row) {
        static $i = 0;
        echo '<div id="kerdes_valasz">';
        echo ++$i . '. ' . $row[0] . '<br/>';
        ?>
        <input type="radio" id="elso" name="<?php echo $i ?>" value="<?php echo $row[1] ?>"/>
        <label for="elso"><?php echo $row[1] ?></label><br/>
        <input type="radio" id="masodik" name="<?php echo $i ?>" value="<?php echo $row[2] ?>"/>
        <label for="masodik"><?php echo $row[2] ?></label><br/>
        <input type="radio" id="harmadik"name="<?php echo $i ?>" value="<?php echo $row[3] ?>"/>
        <label for="harmadik"><?php echo $row[3] ?></label><br/>
        <input type="radio" id="negyedik"name="<?php echo $i ?>" value="<?php echo $row[4] ?>"/>
        <label for="negyedik"><?php echo $row[4] ?></label><br/>
        <?php
        echo '</div>';
    }
?>