<!doctype html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Quiz - Új kérdés felvétele</title>
        <link rel="stylesheet" href="style.css"/>
    </head>
    <body>
        <form method="post" action="index.php?op=ujkerdes" id="ujkerdes">
            <label for="kerdesnev">Kérdés:</label>
            <input type="text" id="kerdesnev" name="kerdesnev"/>
            <label for="temakor">Témakör:</label>
            <input type="text" id="temakor" name="temakor"/>
            <label for="nehezseg">Nehézségi szint: </label>
            <input type="text" id="nehezseg" name="nehezseg"/>
            <label for="jovalasz">Jó válasz:</label>
            <input type="text" id="jovalasz" name="jovalasz"/>
            <label for="kerdesnev">Rossz válasz (1):</label>
            <input type="text" id="valasz1" name="valasz1"/>
            <label for="kerdesnev">Rossz válasz (2):</label>
            <input type="text" id="valasz2" name="valasz2"/>
            <label for="kerdesnev">Rossz válasz (3):</label>
            <input type="text" id="valasz3" name="valasz3"/>
            <input type="submit" id="hozzaad" name="hozzaad" value="Kérdés hozzáadása"/>
        </form>

        <?php
            require_once("functions.php");
            if (isset($_POST['hozzaad'])) {
                $kerdes = htmlspecialchars($_POST['kerdesnev']);
                $temakor = htmlspecialchars($_POST['temakor']);
                $nehezseg = htmlspecialchars($_POST['nehezseg']);
                $jovalasz = htmlspecialchars($_POST['jovalasz']);
                $rossz1 = htmlspecialchars($_POST['valasz1']);
                $rossz2 = htmlspecialchars($_POST['valasz2']);
                $rossz3 = htmlspecialchars($_POST['valasz3']);
                $helyes = false;
                if (($nehezseg !== 'Könnyű') or ($nehezseg !== 'Közepes') or ($nehezseg !== 'Nehéz')) {
                    echo '<div class="error">Nincs ilyen nehézségi szint!</div>';
                    die();
                } else {
					if($nehezseg=='Könnyű'){
						$query = "INSERT INTO Kerdes (kerdes, temakor_id, szint) VALUES ('$kerdes', '$temakor', 1);";
					}else if($nehezseg=='Közepes'){
						$query = "INSERT INTO Kerdes (kerdes, temakor_id, szint) VALUES ('$kerdes', '$temakor', 2);";
					}else{
						$query = "INSERT INTO Kerdes (kerdes, temakor_id, szint) VALUES ('$kerdes', '$temakor', 3);";	
					}
					$stid = oci_parse($c, $query);
					$result = oci_execute($stid);
					
					$query2 ="INSERT INTO Valasz (kerdes_id, valasz, helyes_e) VALUES 
					(20, '$jovalasz', 1),
					(20, '$rossz1', 0),
					(20, '$rossz2', 0),
					(20, '$rossz3', 0);";
					$stid2 = oci_parse($c, $query2);
					$result2 = oci_execute($stid2);
					
                 
                    //$result: lekérdezés 'eredménye' - hibaellenőrzésre
                    if ($result === null or $result2 === null) {
                        adatbazisHiba();
                        die();
                    }
                }
            }
        ?>
    </body>
</html>