<script>
    document.title = "Quiz - Ranglista";
</script>

<?php
    require_once("functions.php");
	include_once("database.php");
	$s = oci_parse($c, 'SELECT sum(pont) AS "pontok", Felhasznalo.nev from Pont, Felhasznalo WHERE Pont.felhasznalo_id = Felhasznalo.id GROUP BY Felhasznalo.nev ORDER BY "pontok"');
	$result = oci_execute($s);
    if ($result === NULL) {
        adatbazisHiba();
        die();
    } else {
        echo '<table id="ranglista">';
        echo '<tr>';
        echo '<th>A játékos neve</th>';
        echo '<th>A játékos pontszáma</th>';
        echo '</tr>';
		
        while($row = oci_fetch_array($s)) {
			echo '<tr>';
            echo '<td>' . $row[1] . '</td>'; 
            echo '<td>' . ($row[0]) . '</td>'; 
			echo '</tr>';
        }
		 
        echo '</table>';
    }
?>